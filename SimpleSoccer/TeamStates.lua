Blah = {}

Blah["Test"] = function() 
	 print ("[Lua]: Test from TeamStates.lua")
end

Blah["Stuff"] = function()
	print ("[Lua]: Stuff from TeamStates.lua")
	return true
end


-------------------------------------------------------------------------------

-- create the Idle state

-------------------------------------------------------------------------------
State_Team_Idle = {}


State_Team_Idle["Enter"] = function(soccerTeam)
   print("Lua Says Enter")
end


State_Team_Idle["Execute"] = function(soccerTeam)
	print("Lua Says TEAM STATES!!!!!!!")
  
end

  
State_Team_Idle["Exit"] = function(soccerTeam)
	print("Lua Says Exit")
end


State_Team_Idle["OnMessage"] = function(soccerTeam, telegram)
	return false;
end


-------------------------------------------------------------------------------

-- create the Defending state

-------------------------------------------------------------------------------
State_Team_Defending = {}


State_Team_Defending["Enter"] = function(soccerTeam)
  print("Lua Says DEFENDING")
  if soccerTeam:TColor() == 0 then
	soccerTeam:SetPlayerHomeRegion(0, 1)
	soccerTeam:SetPlayerHomeRegion(1, 12)
	soccerTeam:SetPlayerHomeRegion(2, 14)
	soccerTeam:SetPlayerHomeRegion(3, 6)
	soccerTeam:SetPlayerHomeRegion(4, 4)
  else
	soccerTeam:SetPlayerHomeRegion(0, 16)
	soccerTeam:SetPlayerHomeRegion(1, 3)
	soccerTeam:SetPlayerHomeRegion(2, 5)
	soccerTeam:SetPlayerHomeRegion(3, 9)
	soccerTeam:SetPlayerHomeRegion(4, 13)
  end 
  soccerTeam:UpdateTargetsOfWaitingPlayers()
  
  
  
end


State_Team_Defending["Execute"] = function(soccerTeam)
  
  
    if soccerTeam:InControl() then
      print("TeamState Says Execute");
     soccerTeam:changeState("State_Team_Attacking")
    end
end

  
State_Team_Defending["Exit"] = function(soccerTeam)
	print("Lua Says Exit")
end


State_Team_Defending["OnMessage"] = function(soccerTeam, telegram)
	return false;
end

-------------------------------------------------------------------------------

-- create the Attacking state

-------------------------------------------------------------------------------
State_Team_Attacking = {}


State_Team_Attacking["Enter"] = function(soccerTeam)
 if soccerTeam:TColor() == 0 then
	soccerTeam:SetPlayerHomeRegion(0, 1)
	soccerTeam:SetPlayerHomeRegion(1, 12)
	soccerTeam:SetPlayerHomeRegion(2, 14)
	soccerTeam:SetPlayerHomeRegion(3, 6)
	soccerTeam:SetPlayerHomeRegion(4, 4)
  else
	soccerTeam:SetPlayerHomeRegion(0, 16)
	soccerTeam:SetPlayerHomeRegion(1, 3)
	soccerTeam:SetPlayerHomeRegion(2, 5)
	soccerTeam:SetPlayerHomeRegion(3, 9)
	soccerTeam:SetPlayerHomeRegion(4, 13)
  end 
  soccerTeam:UpdateTargetsOfWaitingPlayers()
  
  
  
end


State_Team_Attacking["Execute"] = function(soccerTeam)
	
  if soccerTeam:InControl() == false then
        soccerTeam:changeState("State_Team_Defending")
    end
  
  soccerTeam:determineBestSupportingPosition()
  
end

  
State_Team_Attacking["Exit"] = function(soccerTeam)
	--soccerTeam:SetSupportingPlayer(soccerTeam:NULLPOINTER())
end


State_Team_Defending["OnMessage"] = function(soccerTeam, telegram)
	return false;
end

-------------------------------------------------------------------------------

-- create the KICKOFF state

-------------------------------------------------------------------------------
State_Team_KickOff = {}


State_Team_KickOff["Enter"] = function(soccerTeam)
  soccerTeam:SetControllingPlayer(soccerTeam:NULLPOINTER())
  soccerTeam:SetSupportingPlayer(soccerTeam:NULLPOINTER())
  soccerTeam:SetReceiver(soccerTeam:NULLPOINTER())
  soccerTeam:SetPlayerClosestToBall(soccerTeam:NULLPOINTER())
  soccerTeam:ReturnAllFieldPlayersToHome()
end


State_Team_KickOff["Execute"] = function(soccerTeam)
		print("EXECUTING KICK OFF")
 -- if soccerTeam:AllPlayersAtHome()  and soccerTeam:AllOponentsPlayersAtHome() then
    soccerTeam:changeState("State_Team_Defending")
   -- end
  
end

  
State_Team_KickOff["Exit"] = function(soccerTeam)
	soccerTeam:SetGameOn()
end


State_Team_KickOff["OnMessage"] = function(soccerTeam, telegram)
	return false;
end
