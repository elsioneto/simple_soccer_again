Blah = {}

Blah["Test"] = function() 
	 print ("[Lua]: Test from GoalkeeperStates.lua")
end

Blah["Stuff"] = function()
	print ("[Lua]: Stuff from GoalkeeperStates.lua")
	return true
end


-------------------------------------------------------------------------------

-- create the Idle state

-------------------------------------------------------------------------------
State_Goalkeeper_Idle = {}


State_Goalkeeper_Idle["Enter"] = function(goalKeeper)
   print("Lua Says Enter")
end


State_Goalkeeper_Idle["Execute"] = function(goalKeeper)
	print("Lua Says Execute")
end

  
State_Goalkeeper_Idle["Exit"] = function(goalKeeper)
	print("Lua Says Exit")
end


State_Goalkeeper_Idle["OnMessage"] = function(goalKeeper, telegram)
	return false;
end

-------------------------------------------------------------------------------

-- create the Global Keeper state

-------------------------------------------------------------------------------
State_Goalkeeper_Global = {}


State_Goalkeeper_Global["Enter"] = function(GoalKeeper)
   print("Lua Says GlobalKeeperState Enter")
end


State_Goalkeeper_Global["Execute"] = function(GoalKeeper)
	print("Lua Says GlobalKeeperState Execute")
end

  
State_Goalkeeper_Global["Exit"] = function(GoalKeeper)
	print("Lua Says GlobalKeeperState Exit")
end


State_Goalkeeper_Global["OnMessage"] = function(GoalKeeper, telegram)
  print("Lua Says GlobalKeeperState OnMessage")
	if GoalKeeper:TelegramGoHome(telegram) then
        GoalKeeper:SetDefaultHomeRegion()
        GoalKeeper:GetFSM():ChangeState(State_Goalkeeper_ReturnHome)
        return true
    elseif GoalKeeper:TelegramRecieveBall(telegram) then
        GoalKeeper:GetFSM():ChangeState(State_Goalkeeper_InterceptBall)
        return true
    end

	return false
end

-------------------------------------------------------------------------------

-- create the Tend Goal State

-------------------------------------------------------------------------------
State_Goalkeeper_TendGoal = {}


State_Goalkeeper_TendGoal["Enter"] = function(GoalKeeper)
    print("Lua Says TendGoal Enter")

    -- Turn Interpose On
    GoalKeeper:InterposeOn()

	-- interpose will position the agent between the ball position and a target
    -- position situated along the goal mouth. This call sets the target
	GoalKeeper:SteeringSetTarget(GoalKeeper:GetRearInterposeTarget())

end


State_Goalkeeper_TendGoal["Execute"] = function(GoalKeeper)
	-- the rear interpose target will change as the ball's position changes
	-- so it must be updated each update-step 
	GoalKeeper:SteeringSetTarget(GoalKeeper:GetRearInterposeTarget())

	-- if the ball comes in range the keeper traps it and then changes state
	-- to put the ball back in play

   if (GoalKeeper:BallWithinKeeperRange()) then
		  GoalKeeper:TrapBall()
          GoalKeeper:HasBall()
		  GoalKeeper:GetFSM():ChangeState(State_Goalkeeper_PutBallBackInPlay)
		return
   end

	-- if ball is within a predefined distance, the keeper moves out from
	-- position to try and intercept it.

	if (GoalKeeper:BallWithinRangeForIntercept() and not GoalKeeper:TeamInControl()) then
		GoalKeeper:GetFSM():ChangeState(State_Goalkeeper_InterceptBall)
	end

	
	-- if the keeper has ventured too far away from the goal-line and there
	-- is no threat from the opponents he should move back towards it

	if (GoalKeeper:TooFarFromGoalMouth() and GoalKeeper:TeamInControl()) then
		GoalKeeper:GetFSM():ChangeState(State_Goalkeeper_ReturnHome)
		return
	end

end

  
State_Goalkeeper_TendGoal["Exit"] = function(GoalKeeper)
	GoalKeeper:InterposeOff()
end


State_Goalkeeper_TendGoal["OnMessage"] = function(goalKeeper, telegram)
	return false;
end

-------------------------------------------------------------------------------

-- create the ReturnHome state

-------------------------------------------------------------------------------
State_Goalkeeper_ReturnHome = {}


State_Goalkeeper_ReturnHome["Enter"] = function(GoalKeeper)
   GoalKeeper:ArriveOn()
end


State_Goalkeeper_ReturnHome["Execute"] = function(GoalKeeper)
	GoalKeeper:SteeringSetTarget(GoalKeeper:HomeRegionCentre())

	-- if close enough to home or the opponents get control over the ball,
	-- change state to tend goal
	if GoalKeeper:InHomeRegion() or not GoalKeeper:TeamInControl() then
		GoalKeeper:GetFSM():ChangeState(State_Goalkeeper_TendGoal)
	end
end

  
State_Goalkeeper_ReturnHome["Exit"] = function(GoalKeeper)
	GoalKeeper:ArriveOff()
end


State_Goalkeeper_ReturnHome["OnMessage"] = function(goalKeeper, telegram)
	return false;
end

-------------------------------------------------------------------------------

-- create the InterceptBall state

-------------------------------------------------------------------------------
State_Goalkeeper_InterceptBall = {}


State_Goalkeeper_InterceptBall["Enter"] = function(GoalKeeper)
	GoalKeeper:pursuitOn()
end


State_Goalkeeper_InterceptBall["Execute"] = function(GoalKeeper)
  -- if the goalkeeper moves to far away from the goal he should return to his
  -- home region UNLESS he is the closest player to the ball, in which case,
  -- he should keep trying to intercept it.

  if GoalKeeper:TooFarFromGoalMouth() and not GoalKeeper:IsClosestPlayerOnPitchToBall() then
		GoalKeeper:GetFSM():ChangeState(State_Goalkeeper_ReturnHome)
		return
  end
  
   --if the ball becomes in range of the goalkeeper's hands he traps the 
   --ball and puts it back in play
   if GoalKeeper:BallWithinKeeperRange() then
		GoalKeeper:TrapBall()
		GoalKeeper:HasBall()
		GoalKeeper:GetFSM():ChangeState(State_Goalkeeper_PutBallBackInPlay)

		return
   end
end

  
State_Goalkeeper_InterceptBall["Exit"] = function(GoalKeeper)
	GoalKeeper:pursuitOff()
end


State_Goalkeeper_InterceptBall["OnMessage"] = function(goalKeeper, telegram)
	return false;
end

-------------------------------------------------------------------------------

-- create the PutBallBackInPlay state

-------------------------------------------------------------------------------
State_Goalkeeper_PutBallBackInPlay = {}


State_Goalkeeper_PutBallBackInPlay["Enter"] = function(GoalKeeper)
   -- let the team know that the keeper is in control
   GoalKeeper:SetAsControllingPlayer(GoalKeeper)
   
   -- send all the players home
   GoalKeeper:ReturnAllOpponentFieldPlayersHome()
   GoalKeeper:ReturnAllFieldPlayersHome()

end


State_Goalkeeper_PutBallBackInPlay["Execute"] = function(GoalKeeper)
	
  local ballTarget = GoalKeeper:CreateVector(1.0,1.0)

  local receiver = GoalKeeper:NULLPOINTER()
  GoalKeeper:FindPassForGoalie(receiver, ballTarget)

  -- test if there are players further forward on the field we might
  -- be able to pass to. If so, make a pass.
  if receiver ~= 0 then
	-- make the pass   
	GoalKeeper:KeeperKick(ballTarget)

    -- goalkeeper no longer has ball 
	GoalKeeper:LoseBall()

    -- let the receiving player know the ball's comin' at him
     --Dispatcher->DispatchMsg(SEND_MSG_IMMEDIATELY, keeper->ID(), receiver->ID(), Msg_ReceiveBall, &ballTarget);
     --GoalKeeper:GoaliePassTelegram(receiver, ballTarget)  

    -- go back to tending the goal   
	GoalKeeper:GetFSM():ChangeState(State_Goalkeeper_TendGoal)

    --return
  end

  GoalKeeper:ResetVelocity()

end

  
State_Goalkeeper_PutBallBackInPlay["Exit"] = function(GoalKeeper)
	print("Lua Says Exit")
end


State_Goalkeeper_PutBallBackInPlay["OnMessage"] = function(goalKeeper, telegram)
	return false;
end