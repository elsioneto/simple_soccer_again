#include "PlayerBase.h"
#include "SteeringBehaviors.h"
#include "2D/Transformations.h"
#include "2D/Geometry.h"
#include "misc/Cgdi.h"
#include "2D/C2DMatrix.h"
#include "Game/Region.h"
#include "ParamLoader.h"
#include "Messaging/MessageDispatcher.h"
#include "SoccerMessages.h"
#include "SoccerTeam.h"
#include "FieldPlayerStates.h"
#include "GoalKeeperStates.h"

#include "ParamLoader.h"
#include "Goal.h"
#include "SoccerBall.h"
#include "SoccerPitch.h"
#include "Debug/DebugConsole.h"


using std::vector;

#ifdef LUA
extern "C" {
#include <lua.h>
#include <lualib.h>
#include <lauxlib.h>
}
#include <luabind/luabind.hpp>
using namespace luabind;
#include "LuaHelperFunctions.h"

void RegisterPlayerBase(lua_State *lua) {

	RegisterMovingEntity(lua);
	module(lua)[
		class_<PlayerBase, MovingEntity>("PlayerBase")

			//=======================
			// Shared Methods
			//=======================
			.def("BallWithinReceivingRange", &PlayerBase::BallWithinReceivingRange)
			.def("isControllingPlayer", &PlayerBase::isControllingPlayer)
			.def("BallWithinKickingRange", &PlayerBase::BallWithinKickingRange)
			.def("isClosestTeamMemberToBall", &PlayerBase::isClosestTeamMemberToBall)
			.def("BallPos", &PlayerBase::BallPos)
			.def("SteeringSetTarget", &PlayerBase::SteeringSetTarget)
			.def("TrackBall", &PlayerBase::TrackBall)
			.def("FindSupport", &PlayerBase::FindSupport)
			.def("SeekOn", &PlayerBase::SeekOn)
			.def("SeekOff", &PlayerBase::SeekOff)
			.def("ArriveOn", &PlayerBase::ArriveOn)
			.def("ArriveOff", &PlayerBase::ArriveOff)
			.def("SetControllingPlayer", &PlayerBase::SetControllingPlayer)
			.def("dotProduct", &PlayerBase::dotProduct)
			.def("receiver", &PlayerBase::receiver)
			.def("goalKeeperHalBall", &PlayerBase::goalKeeperHalBall)
			.def("NULLPOINTER", &PlayerBase::NULLPOINTER)
			.def("canShoot", &PlayerBase::canShoot)
			.def("RandomFloat", &PlayerBase::RandomFloat)
			.def("getOponentGoalPos", &PlayerBase::getOponentGoalPos)
			.def("returnNoise", &PlayerBase::returnNoise)
			.def("KickBall", &PlayerBase::KickBall)
			.def("kickDirection", &PlayerBase::kickDirection)
			.def("isThreatened", &PlayerBase::isThreatened)
			.def("FindPass", &PlayerBase::FindPass)
			.def("sendPassMessage", &PlayerBase::sendPassMessage)
			.def("GetSupportSpot", &PlayerBase::GetSupportSpot)
			.def("GetSteeringTarget", &PlayerBase::GetSteeringTarget)
			.def("AtTarget", &PlayerBase::AtTarget)
			.def("SetSupportingPlayer", &PlayerBase::SetSupportingPlayer)
			.def("GameOn", &PlayerBase::GameOn)
			.def("GetOwnPos", &PlayerBase::GetOwnPos)
			.def("GetHomeRegionCenter", &PlayerBase::GetHomeRegionCenter)
			.def("isAheadOfAttacker", &PlayerBase::isAheadOfAttacker)
			.def("GetFacing", &PlayerBase::GetFacing)
			.def("GetHeading", &PlayerBase::GetHeading)
			.def("AngleBetweenPlayerAndHeading", &PlayerBase::AngleBetweenPlayerAndHeading)
			.def("RotateAroundOrigin", &PlayerBase::RotateAroundOrigin)
			.def("GetHomeGoalFacing", &PlayerBase::GetHomeGoalFacing)
			.def("SetReceiver", &PlayerBase::SetReceiver)
			.def("InHotRegion", &PlayerBase::InHotRegion)
			.def("isOpponentWithinRadius", &PlayerBase::isOpponentWithinRadius)
			.def("pursuitOn", &PlayerBase::pursuitOn)
			.def("inControl", &PlayerBase::inControl)
			.def("pursuitOff", &PlayerBase::pursuitOff)

			//=======================
			// Goalkeeper Methods
			//=======================
			.def("InterposeOn", &PlayerBase::InterposeOn)
			.def("InterposeOff", &PlayerBase::InterposeOff)
			.def("BallWithinKeeperRange", &PlayerBase::BallWithinKeeperRange)
			.def("TrapBall", &PlayerBase::TrapBall)
			.def("HasBall", &PlayerBase::HasBall)
			.def("LoseBall", &PlayerBase::LoseBall)
			.def("TeamInControl", &PlayerBase::TeamInControl)
			.def("HomeRegionCentre", &PlayerBase::HomeRegionCentre)
			.def("InHomeRegion", &PlayerBase::InHomeRegion)
			.def("IsClosestPlayerOnPitchToBall", &PlayerBase::isClosestPlayerOnPitchToBall)
			.def("SetAsControllingPlayer", &PlayerBase::SetAsControllingPlayer)
			.def("ReturnAllFieldPlayersHome", &PlayerBase::ReturnAllFieldPlayersHome)
			.def("ReturnAllOpponentFieldPlayersHome", &PlayerBase::ReturnAllOpponentFieldPlayersHome)
			.def("ResetVelocity", &PlayerBase::ResetVelocity)
			.def("FindPassForGoalie", &PlayerBase::FindPassForGoalie)
			.def("KeeperKick", &PlayerBase::KeeperKick)

			//=======================
			// Telegram Methods
			//=======================
			.def("GoaliePassTelegram", &PlayerBase::GoaliePassTelegram)
			.def("SetDefaultHomeRegion", &PlayerBase::SetDefaultHomeRegion)
			.def("TelegramGoHome", &PlayerBase::TelegramGoHome)
			.def("TelegramReceiveBall", &PlayerBase::TelegramReceiveBall)
			.def("TelegramSupportAttacker", &PlayerBase::TelegramSupportAttacker)
			.def("TelegramPassToMe", &PlayerBase::TelegramPassToMe)
			.def("TelegramWait", &PlayerBase::TelegramWait)
			//.def("GetExtraInfo", &PlayerBase::GetExtraInfo)
			//.def("GetFieldPlayerFromExtraInfo", &PlayerBase::GetFieldPlayerFromExtraInfo)
			.def("subtractVectors", &PlayerBase::subtractVectors)
			.def("receiverPos", &PlayerBase::receiverPos)
			.def("GetExtraInfo", &PlayerBase::GetExtraInfo)	
	];
}
#endif

//----------------------------- dtor -------------------------------------
//------------------------------------------------------------------------
PlayerBase::~PlayerBase() 
{
  delete m_pSteering;
}

//----------------------------- ctor -------------------------------------
//------------------------------------------------------------------------
PlayerBase::PlayerBase(SoccerTeam* home_team,
                       int   home_region,
                       Vector2D  heading,
                       Vector2D velocity,
                       double    mass,
                       double    max_force,
                       double    max_speed,
                       double    max_turn_rate,
                       double    scale,
                       player_role role):    

    MovingEntity(home_team->Pitch()->GetRegionFromIndex(home_region)->Center(),
                 scale*10.0,
                 velocity,
                 max_speed,
                 heading,
                 mass,
                 Vector2D(scale,scale),
                 max_turn_rate,
                 max_force),
   m_pTeam(home_team),
   m_dDistSqToBall(MaxFloat),
   m_iHomeRegion(home_region),
   m_iDefaultRegion(home_region),
   m_PlayerRole(role)
{
  
  //setup the vertex buffers and calculate the bounding radius
  const int NumPlayerVerts = 4;
  const Vector2D player[NumPlayerVerts] = {Vector2D(-3, 8),
                                           Vector2D(3,	10),
                                           Vector2D(3,	-10),
                                           Vector2D(-3,	-8)};

  for (int vtx=0; vtx<NumPlayerVerts; ++vtx)
  {
    m_vecPlayerVB.push_back(player[vtx]);

    //set the bounding radius to the length of the 
    //greatest extent
    if (abs(player[vtx].x) > m_dBoundingRadius)
    {
      m_dBoundingRadius = abs(player[vtx].x);
    }

    if (abs(player[vtx].y) > m_dBoundingRadius)
    {
      m_dBoundingRadius = abs(player[vtx].y);
    }
  }

  //set up the steering behavior class
  m_pSteering = new SteeringBehaviors(this,
                                      m_pTeam->Pitch(),
                                      Ball());  
  
 

  //a player's start target is its start position (because it's just waiting)
  m_pSteering->SetTarget(home_team->Pitch()->GetRegionFromIndex(home_region)->Center());
  //m_pSteering->SetTarget(Vector2D(50,0));
}


void PlayerBase::SetSupportingPlayer(PlayerBase* p)
{
	Team()->SetSupportingPlayer(p);
}

void PlayerBase::SteeringSetTarget(Vector2D v) {
	
	m_pSteering->SetTarget(v);
	
}

Vector2D PlayerBase::GetHomeRegionCenter()
{
	return HomeRegion()->Center();
}

Vector2D PlayerBase::GetSupportSpot()
{
	//m_pSupportSpotCalc = new SupportSpotCalculator(Pos().x,Pos().y, Team());
	Vector2D v = Team()->GetSupportSpot();
	return v;
	//return Vector2D(0,0);
}

//----------------------------- TrackBall --------------------------------
//
//  sets the player's heading to point at the ball
//------------------------------------------------------------------------
void PlayerBase::TrackBall()
{
  RotateHeadingToFacePosition(Ball()->Pos());   
}

//----------------------------- TrackTarget --------------------------------
//
//  sets the player's heading to point at the current target
//------------------------------------------------------------------------
void PlayerBase::TrackTarget()
{
  SetHeading(Vec2DNormalize(Steering()->Target() - Pos()));
}

bool PlayerBase::inControl()
{
	return Team()->InControl();
	
}

//------------------------------------------------------------------------
//
//binary predicates for std::sort (see CanPassForward/Backward)
//------------------------------------------------------------------------
bool  SortByDistanceToOpponentsGoal(const PlayerBase*const p1,
                                    const PlayerBase*const p2)
{
  return (p1->DistToOppGoal() < p2->DistToOppGoal());
}

bool  SortByReversedDistanceToOpponentsGoal(const PlayerBase*const p1,
                                            const PlayerBase*const p2)
{
  return (p1->DistToOppGoal() > p2->DistToOppGoal());
}

void PlayerBase::SeekOn()
{
	m_pSteering->SeekOn();
}

void PlayerBase::SeekOff()
{
	m_pSteering->SeekOff();
}

//------------------------- WithinFieldOfView ---------------------------
//
//  returns true if subject is within field of view of this player
//-----------------------------------------------------------------------
bool PlayerBase::PositionInFrontOfPlayer(Vector2D position)const
{
  Vector2D ToSubject = position - Pos();

  if (ToSubject.Dot(Heading()) > 0) 
    
    return true;

  else

    return false;
}

//------------------------- IsThreatened ---------------------------------
//
//  returns true if there is an opponent within this player's 
//  comfort zone
//------------------------------------------------------------------------
bool PlayerBase::isThreatened() const
{
  //check against all opponents to make sure non are within this
  //player's comfort zone
  std::vector<PlayerBase*>::const_iterator curOpp;  
  curOpp = Team()->Opponents()->Members().begin();
 
  for (curOpp; curOpp != Team()->Opponents()->Members().end(); ++curOpp)
  {
    //calculate distance to the player. if dist is less than our
    //comfort zone, and the opponent is infront of the player, return true
    if (PositionInFrontOfPlayer((*curOpp)->Pos()) &&
       (Vec2DDistanceSq(Pos(), (*curOpp)->Pos()) < Prm.PlayerComfortZoneSq))
    {        
      return true;
    }
   
  }// next opp

  return false;
}


bool PlayerBase::FindPass(PlayerBase * receiver, Vector2D ballTarget, double power, int MinPassDist)
{

	return m_pTeam->FindPass(this, receiver, ballTarget, power, MinPassDist);
}

bool PlayerBase::FindPassForGoalie(PlayerBase * receiver, Vector2D ballTarget) {
	return m_pTeam->FindPass(this, receiver, ballTarget, Prm.MaxPassingForce, Prm.GoalkeeperMinPassDist);
}


//----------------------------- FindSupport -----------------------------------
//
//  determines the player who is closest to the SupportSpot and messages him
//  to tell him to change state to SupportAttacker
//-----------------------------------------------------------------------------
void PlayerBase::FindSupport()const
{    
  //if there is no support we need to find a suitable player.
  if (Team()->SupportingPlayer() == NULL)
  {
	  if (Team()->DetermineBestSupportingAttacker() == NULL)
		  return;


	  if (Team()->DetermineBestSupportingAttacker() != NULL)
	  {
		  PlayerBase* BestSupportPly = Team()->DetermineBestSupportingAttacker();

		  Team()->SetSupportingPlayer(BestSupportPly);

		  Dispatcher->DispatchMsg(SEND_MSG_IMMEDIATELY,
			  ID(),
			  Team()->SupportingPlayer()->ID(),
			  Msg_SupportAttacker,
			  NULL);
	  }
  }
    
  PlayerBase* BestSupportPly = Team()->DetermineBestSupportingAttacker();
    
  //if the best player available to support the attacker changes, update
  //the pointers and send messages to the relevant players to update their
  //states
  if (BestSupportPly && (BestSupportPly != Team()->SupportingPlayer()))
  {
    
    if (Team()->SupportingPlayer())
    {
      Dispatcher->DispatchMsg(SEND_MSG_IMMEDIATELY,
                              ID(),
                              Team()->SupportingPlayer()->ID(),
                              Msg_GoHome,
                              NULL);
    }
    
    
    
    Team()->SetSupportingPlayer(BestSupportPly);

    Dispatcher->DispatchMsg(SEND_MSG_IMMEDIATELY,
                            ID(),
                            Team()->SupportingPlayer()->ID(),
                            Msg_SupportAttacker,
                            NULL);
  }
}

//calculate distance to opponent's goal. Used frequently by the passing//methods
double PlayerBase::DistToOppGoal()const
{
  return fabs(Pos().x - Team()->OpponentsGoal()->Center().x);
}

//=======================
// Shared Methods
//=======================

void LookAtBall()
{
	//KickBall::Instance();
}

void PlayerBase::KickBall(Vector2D direction, double force)
{

	Ball()->Kick(direction, force);
}

Vector2D PlayerBase::kickDirection(Vector2D ballTarget, Vector2D ballPos)
{
	return ballTarget - ballPos;
}

Vector2D PlayerBase::BallPos()
{
	return Ball()->Pos();
}

double PlayerBase::DistToHomeGoal()const
{
  return fabs(Pos().x - Team()->HomeGoal()->Center().x);
}

Vector2D PlayerBase::CenterPos()
{
	return Team()->HomeGoal()->Center();
}

void PlayerBase::ArriveOn()
{
	m_pSteering->ArriveOn();
}

void PlayerBase::ArriveOff()
{
	m_pSteering->ArriveOff();
}

bool PlayerBase::isControllingPlayer()const
{return Team()->ControllingPlayer()==this;}

bool PlayerBase::BallWithinKeeperRange()const
{
  return (Vec2DDistanceSq(Pos(), Ball()->Pos()) < Prm.KeeperInBallRangeSq);
}

bool PlayerBase::BallWithinReceivingRange()const
{
  return (Vec2DDistanceSq(Pos(), Ball()->Pos()) < Prm.BallWithinReceivingRangeSq);
}

bool PlayerBase::BallWithinKickingRange()const
{
  return (Vec2DDistanceSq(Ball()->Pos(), Pos()) < Prm.PlayerKickingDistanceSq);
}

bool PlayerBase::canShoot(Vector2D pos, double power, Vector2D ballTarget)
{
	return Team()->CanShoot(pos, power, ballTarget);
}

Vector2D PlayerBase::returnNoise(Vector2D ballTarget)
{
	double displacement = (Pi - Pi*Prm.PlayerKickingAccuracy) * RandomClamped();

	Vector2D toTarget = ballTarget - BallPos();

	Vec2DRotateAroundOrigin(toTarget, displacement);

	return toTarget + BallPos();
}

bool PlayerBase::GameOn()
{
	return Pitch()->GameOn();
}

void PlayerBase::sendPassMessage(PlayerBase* receiver, Vector2D BallTarget)
{
	if ( receiver != NULL)
	{
			Dispatcher->DispatchMsg(SEND_MSG_IMMEDIATELY,
				this->ID(),
				receiver->ID(),
				Msg_ReceiveBall,
				&BallTarget);

			Team()->SetReceiver(receiver);
	}
}

bool PlayerBase::InHomeRegion()const
{
  if (m_PlayerRole == goal_keeper)
  {
    return Pitch()->GetRegionFromIndex(m_iHomeRegion)->Inside(Pos(), Region::normal);
  }
  else
  {
    return Pitch()->GetRegionFromIndex(m_iHomeRegion)->Inside(Pos(), Region::halfsize);
  }
}

bool PlayerBase::AtTarget()const
{
  return (Vec2DDistanceSq(Pos(), Steering()->Target()) < Prm.PlayerInTargetRangeSq);
}

Vector2D PlayerBase::GetSteeringTarget()
{
	return Steering()->Target();
}

bool PlayerBase::isClosestTeamMemberToBall()const
{
  return Team()->PlayerClosestToBall() == this;
}

bool PlayerBase::isClosestPlayerOnPitchToBall()const
{
  return isClosestTeamMemberToBall() && 
         (DistSqToBall() < Team()->Opponents()->ClosestDistToBallSq());
}

Vector2D PlayerBase::getOponentGoalPos()
{
	return Team()->Opponents()->HomeGoal()->Center();
}

Vector2D PlayerBase::GetOwnPos()
{
	return this->Pos();
}

double PlayerBase::dotProduct()
{
	Vector2D ToBall = Ball()->Pos() - Pos();
	double   dot = Heading().Dot(Vec2DNormalize(ToBall));
	return dot;
}
bool PlayerBase::goalKeeperHalBall()
{
	return Pitch()->GoalKeeperHasBall();
}

PlayerBase* PlayerBase::receiver()
{
	PlayerBase* r = Team()->Receiver();
	return r;
}

bool PlayerBase::InHotRegion() const
{
  return fabs(Pos().y - Team()->OpponentsGoal()->Center().y ) <
         Pitch()->PlayingArea()->Length()/3.0;
}

bool PlayerBase::isAheadOfAttacker()const
{
  return fabs(Pos().x - Team()->OpponentsGoal()->Center().x) <
         fabs(Team()->ControllingPlayer()->Pos().x - Team()->OpponentsGoal()->Center().x);
}

SoccerBall* const PlayerBase::Ball()const
{
  return Team()->Pitch()->Ball();
}

SoccerPitch* const PlayerBase::Pitch()const
{
  return Team()->Pitch();
}

const Region* const PlayerBase::HomeRegion()const
{
  return Pitch()->GetRegionFromIndex(m_iHomeRegion);
}

void PlayerBase::SetControllingPlayer(PlayerBase* fp)
{
	Team()->SetControllingPlayer(this);
	
}

double PlayerBase::GetFacing()
{
	return Team()->HomeGoal()->Facing().Dot(this->Heading());
}

Vector2D PlayerBase::GetHeading()
{
	return Heading();
}

double PlayerBase::AngleBetweenPlayerAndHeading(Vector2D heading)
{
	return QuarterPi * -1 * Team()->HomeGoal()->Facing().Sign(heading);
}

void PlayerBase::RotateAroundOrigin(Vector2D vec, double angle)
{
	Vec2DRotateAroundOrigin(vec, angle);
}

Vector2D PlayerBase::GetHomeGoalFacing()
{
	Vector2D V = Team()->HomeGoal()->Facing();
	return V;
}

void PlayerBase::SetReceiver(PlayerBase * p)
{
	Team()->SetReceiver(p);
}

bool PlayerBase::isOpponentWithinRadius(Vector2D pos, double Radius)
{
	return Team()->isOpponentWithinRadius(pos, Radius);
}

void PlayerBase::pursuitOn()
{
	Steering()->PursuitOn();
}

void PlayerBase::pursuitOff()
{
	Steering()->PursuitOff();
}

Vector2D PlayerBase::subtractVectors(Vector2D v1, Vector2D v2)
{
	return v1 - v2;
}

Vector2D PlayerBase::receiverPos()
{
	return Team()->Receiver()->Pos();
}

//=======================
// Goalkeeper Methods
//=======================
void PlayerBase::InterposeOn() {
	m_pSteering->InterposeOn(Prm.GoalKeeperTendingDistance);
}

void PlayerBase::InterposeOff() {
	m_pSteering->InterposeOff();
}

void PlayerBase::TrapBall() {
	Ball()->Trap();
}

void PlayerBase::HasBall() {
	Pitch()->SetGoalKeeperHasBall(true);
}

void PlayerBase::LoseBall() {
	Pitch()->SetGoalKeeperHasBall(false);
}

bool PlayerBase::TeamInControl() {
	return Team()->InControl();
}

Vector2D PlayerBase::HomeRegionCentre() {
	return HomeRegion()->Center();
}

void PlayerBase::SetAsControllingPlayer(PlayerBase* g) {
	Team()->SetControllingPlayer(g);
}

void PlayerBase::ReturnAllFieldPlayersHome() {
	Team()->ReturnAllFieldPlayersToHome();
}

void PlayerBase::ReturnAllOpponentFieldPlayersHome() {
	Team()->Opponents()->ReturnAllFieldPlayersToHome();
}

void PlayerBase::ResetVelocity() {
	SetVelocity(Vector2D());
}

void PlayerBase::KeeperKick(Vector2D ballTarget) {
	Ball()->Kick(Vec2DNormalize(ballTarget - Ball()->Pos()), Prm.MaxPassingForce);
}

//=======================
// Telegram Methods
//=======================
void PlayerBase::GoaliePassTelegram(int receiver, Vector2D ballTarget) {
	Dispatcher->DispatchMsg(
		SEND_MSG_IMMEDIATELY, 
		this->ID(),
		receiver,
		Msg_ReceiveBall,
		&ballTarget);

}

bool PlayerBase::TelegramReceiveBall(const Telegram& t) {
	return (t.Msg == Msg_ReceiveBall);
}

bool PlayerBase::TelegramSupportAttacker(const Telegram& t) {
	return (t.Msg == Msg_SupportAttacker);
}

bool PlayerBase::TelegramWait(const Telegram& t) {
	return (t.Msg == Msg_Wait);
}

bool PlayerBase::TelegramGoHome(const Telegram& t) {

	return (t.Msg == Msg_GoHome);
}

bool PlayerBase::TelegramPassToMe(const Telegram& t) {
	return (t.Msg == Msg_PassToMe);
}

Vector2D PlayerBase::GetExtraInfo(const Telegram& t)
{
	if (t.ExtraInfo == NULL)
		return Vector2D(500, 150);
	return *(static_cast<Vector2D*>(t.ExtraInfo));
}

