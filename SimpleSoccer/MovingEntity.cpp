#include "Game/MovingEntity.h"

#ifdef LUA
extern "C" {
#include <lua.h>
#include <lualib.h>
#include <lauxlib.h>
}
#include <luabind/luabind.hpp>
using namespace luabind;
#include "LuaHelperFunctions.h"

void RegisterMovingEntity(lua_State *lua) {

	//TODO: Add parent classes and defs for relevant methods.
	module(lua)[
		class_<MovingEntity>("MovingEntity")
			.def("SetMaxSpeed", &MovingEntity::SetMaxSpeed)
			.def("SetVelocity", &MovingEntity::SetVelocity)
			.def("CreateVector", &MovingEntity::CreateVector)
	];
}

#endif