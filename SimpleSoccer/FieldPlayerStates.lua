require("mobdebug").start()

-- Paramaters
PlayerMaxSpeedWithBall                        = 1.2
PlayerMaxSpeedWithoutBall                     = 1.6
PlayerMaxShootingForce                        = 10.0
PlayerChanceAttemptPotShot                    = 0.01
PlayerMaxPassingForce                         = 3.0
PlayerMaxDribbleForce                         = 2.5
PlayerMaxTurnRate                             = 0.4
PlayerMaxComfortZone                          = 60
PlayerMinPassDist                             = 120
PlayerGoalkeepMinPassDist                     = 50
PlayerChanceOfUsingArriveTypeReceiveBehavior  = 0.9

Blah = {}

Blah["Test"] = function() 
	 print ("[Lua]: Test from FieldPlayerStates.lua")
end

Blah["Stuff"] = function()
	print ("[Lua]: Stuff from FieldPlayerStates.lua")
	return true
end

-------------------------------------------------------------------------------

-- create the Idle state

-------------------------------------------------------------------------------
State_FieldPlayer_Idle = {}


State_FieldPlayer_Idle["Enter"] = function(fieldPlayer)
   print("Lua Says teste idle")
end


State_FieldPlayer_Idle["Execute"] = function(fieldPlayer)
  
end

  
State_FieldPlayer_Idle["Exit"] = function(fieldPlayer)
	print("Lua Says Exit")
end


State_FieldPlayer_Idle["OnMessage"] = function(fieldPlayer, telegram)
	return false;
end


-------------------------------------------------------------------------------

-- create the Global Field Player state

-------------------------------------------------------------------------------
State_FieldPlayer_Global = {}


State_FieldPlayer_Global["Enter"] = function(fieldPlayer)
   print("Lua Says Entering")
end


State_FieldPlayer_Global["Execute"] = function(fieldPlayer)
	
	if fieldPlayer:BallWithinReceivingRange() and fieldPlayer:isControllingPlayer() then
    
	  fieldPlayer:SetMaxSpeed(PlayerMaxSpeedWithBall)
    else
      fieldPlayer:SetMaxSpeed(PlayerMaxSpeedWithoutBall)
	end
  
end

  
State_FieldPlayer_Global["Exit"] = function(fieldPlayer)
  print("SUPPORTING ATTACK")
	fieldPlayer:changeState("State_FieldPlayer_SupportAttack");
end


State_FieldPlayer_Global["OnMessage"] = function(fieldPlayer, telegram)
   print("EXECUTING TELEGRAM STATE")
   if fieldPlayer:TelegramGoHome(telegram) then
       print("Go Home")
       local vector = fieldPlayer:GetExtraInfo(telegram)
       fieldPlayer:SteeringSetTarget(vector)
       fieldPlayer:changeState("State_FieldPlayer_ReceiveBall")
       return true
    elseif fieldPlayer:TelegramSupportAttacker(telegram) then
       print("Support Attacker")
       fieldPlayer:SteeringSetTarget(fieldPlayer:GetSupportSpot())
       fieldPlayer:changeState("State_FieldPlayer_SupportAttack")
       return true
    elseif fieldPlayer:TelegramWait(telegram) then
       print("Wait")
       fieldPlayer:changeState("State_FieldPlayer_Wait")
       return true
    elseif fieldPlayer:TelegramGoHome(telegram) then
       print("Go home Execute")
      --fieldPlayer:SetDefaultHomeRegion()
       fieldPlayer:changeState("State_FieldPlayer_ReturnToHome")
       return true
    elseif fieldPlayer:TelegramPassToMe(telegram) then
      print("Pass To Me")
      -- local player = fieldPlayer:GetFieldPlayerFromExtraInfo(telegram)
      if (not fieldPlayer:receiver() == fieldPlayer:NULLPOINTER())then 
       
       -- and fieldPlayer:BallWithinKickingRange() 
       local ballPos = fieldPlayer:BallPos()
      
       
         local receiverPos = fieldPlayer:receiverPos()
         local subtract = fieldPlayer:subtractVectors(fieldPlayer:receiverPos(), fieldPlayer:BallPos())
         fieldPlayer:KickBall(subtract,PlayerMaxPassingForce)
       
       return true
         end
    end
 print("EXECUTING TELEGRAM STATE END")
	return false;
end

-------------------------------------------------------------------------------

-- create the Chase state

-- if fieldPlayer:isClosestTeamMemberToBall() then
		-- fieldPlayer:SteeringSetTarget(fieldPlayer:BallPos())
	-- end

-------------------------------------------------------------------------------
State_FieldPlayer_Chase = {}


State_FieldPlayer_Chase["Enter"] = function(fieldPlayer)
    fieldPlayer:SeekOn()
end


State_FieldPlayer_Chase["Execute"] = function(fieldPlayer)
  
  
 if (fieldPlayer:BallWithinKickingRange()) then
  fieldPlayer:changeState("State_FieldPlayer_KickBall");
    
 end
  
  
 if fieldPlayer:isClosestTeamMemberToBall() then
   
		 fieldPlayer:SteeringSetTarget(fieldPlayer:BallPos())
	
 else
    fieldPlayer:changeState("State_FieldPlayer_ReturnToHome");
   end
end

  
State_FieldPlayer_Chase["Exit"] = function(fieldPlayer)
	fieldPlayer:SeekOff()
end


State_FieldPlayer_Chase["OnMessage"] = function(fieldPlayer, telegram)
	return false;
end

-------------------------------------------------------------------------------

-- create the Support Attack state

-- if fieldPlayer:isClosestTeamMemberToBall() then
		-- fieldPlayer:SteeringSetTarget(fieldPlayer:BallPos())
	-- end

-------------------------------------------------------------------------------
State_FieldPlayer_SupportAttack = {}


State_FieldPlayer_SupportAttack["Enter"] = function(fieldPlayer)
   fieldPlayer:ArriveOn()
   
   local supportSpot = fieldPlayer:GetSupportSpot()
   
   fieldPlayer:SteeringSetTarget(supportSpot)
   
end


State_FieldPlayer_SupportAttack["Execute"] = function(fieldPlayer)
    

    if not( fieldPlayer:inControl()) then
       fieldPlayer:changeState("State_FieldPlayer_ReturnToHome");
       return
    end
    
    if not fieldPlayer:GetSupportSpot() ==  fieldPlayer:GetSteeringTarget() then
       fieldPlayer:SteeringSetTarget(supportSpot)
       fieldPlayer:ArriveOn()
    end
    
    if fieldPlayer:AtTarget() then
      
      fieldPlayer:ArriveOff()
      fieldPlayer:TrackBall()
      
      local vector = fieldPlayer:CreateVector(0,0)
      fieldPlayer:SetVelocity(vector)
      
      if not fieldPlayer:isThreatened() then
      
        local p = fieldPlayer:GetOwnReference()
        fieldPlayer:RequestPass(p)
      end
      
    end
    
end

  
State_FieldPlayer_SupportAttack["Exit"] = function(fieldPlayer)
	
  
  fieldPlayer:SetSupportingPlayer(fieldPlayer:NULLPOINTER())
  fieldPlayer:ArriveOff()
  
  
end


State_FieldPlayer_SupportAttack["OnMessage"] = function(fieldPlayer, telegram)
	return false;
end

-------------------------------------------------------------------------------

-- create the Return to Home Region state

-------------------------------------------------------------------------------
State_FieldPlayer_ReturnToHome = {}


State_FieldPlayer_ReturnToHome["Enter"] = function(fieldPlayer)
  fieldPlayer:ArriveOn()
  
  if (fieldPlayer:InHomeRegion()) then
    fieldPlayer:SteeringSetTarget(fieldPlayer:HomeRegionCentre())
    
  end
  
   print("Lua Says Enter")
end


State_FieldPlayer_ReturnToHome["Execute"] = function(fieldPlayer)
 
  if fieldPlayer:GameOn() then
      if  fieldPlayer:isClosestTeamMemberToBall() and (fieldPlayer:receiver() == fieldPlayer:NULLPOINTER()) and fieldPlayer:goalKeeperHalBall() == false then
        
          fieldPlayer:changeState("State_FieldPlayer_Chase");
        return 
      end
    end
 
  if fieldPlayer:GameOn() and fieldPlayer:InHomeRegion() then
    fieldPlayer:SteeringSetTarget(fieldPlayer:GetOwnPos())
    fieldPlayer:changeState("State_FieldPlayer_Wait");
  else 
      if ((fieldPlayer:GameOn() == false) and fieldPlayer:AtTarget()) then
        fieldPlayer:changeState("State_FieldPlayer_Wait");
      end
  end
 
 
end

  
State_FieldPlayer_ReturnToHome["Exit"] = function(fieldPlayer)
	 fieldPlayer:ArriveOff()
end


State_FieldPlayer_ReturnToHome["OnMessage"] = function(fieldPlayer, telegram)
	return false;
end
-------------------------------------------------------------------------------

-- create the Return to Wait state


-------------------------------------------------------------------------------
State_FieldPlayer_Wait = {}


State_FieldPlayer_Wait["Enter"] = function(fieldPlayer)
  if fieldPlayer:GameOn() == false then
    fieldPlayer:SteeringSetTarget(fieldPlayer:GetHomeRegionCenter())
  end
end


State_FieldPlayer_Wait["Execute"] = function(fieldPlayer)

  if not fieldPlayer:AtTarget() == false then
      fieldPlayer:ArriveOn()
      return
    else
    fieldPlayer:ArriveOff()
    local vector = fieldPlayer:CreateVector(0,0)
    fieldPlayer:SetVelocity(vector)
    fieldPlayer:TrackBall()
  end
  
  if fieldPlayer:inControl() and (fieldPlayer:isControllingPlayer() == false ) and fieldPlayer:isAheadOfAttacker() then
    
      fieldPlayer:RequestPass(fieldPlayer:GetOwnReference())
    return
  end
  
  if fieldPlayer:isClosestTeamMemberToBall() and (fieldPlayer:receiver() == fieldPlayer:NULLPOINTER())  and fieldPlayer:goalKeeperHalBall() == false then
    fieldPlayer:changeState("State_FieldPlayer_Chase")
  end
  
end

  
State_FieldPlayer_Wait["Exit"] = function(fieldPlayer)
	print("Lua Says Exit")
end


State_FieldPlayer_Wait["OnMessage"] = function(fieldPlayer, telegram)
	return false;
end
-------------------------------------------------------------------------------

-- create the Kick Ball state

-------------------------------------------------------------------------------
State_FieldPlayer_KickBall = {}


State_FieldPlayer_KickBall["Enter"] = function(fieldPlayer)
  
   local v = fieldPlayer:GetOwnReference()
   fieldPlayer:SetControllingPlayer(v)
   
   if fieldPlayer:isReadyForNextKick() then
       fieldPlayer:changeState("State_FieldPlayer_Chase");
    end
   
end


State_FieldPlayer_KickBall["Execute"] = function(fieldPlayer)


  local v = fieldPlayer:GetOwnReference()

  local dot = fieldPlayer:dotProduct()
  
  local t = fieldPlayer:NULLPOINTER()
  local ballTarget = fieldPlayer:getOponentGoalPos()
  local power = PlayerMaxShootingForce * dot
  local ballPos = fieldPlayer:BallPos()
  
  if not (fieldPlayer:receiver() == fieldPlayer:NULLPOINTER()) or (fieldPlayer:goalKeeperHalBall()) or (dot < 0) then
    
     fieldPlayer:changeState("State_FieldPlayer_Chase")
   else
     
        if  fieldPlayer:canShoot(ballPos,PlayerMaxShootingForce,ballTarget) or fieldPlayer:RandomFloat() <PlayerChanceAttemptPotShot then
        
           ballTarget = fieldPlayer:returnNoise(ballTarget)
           local kickDirection = fieldPlayer:kickDirection(ballTarget, ballPos)
           
           fieldPlayer:KickBall(kickDirection, power)
           fieldPlayer:changeState("State_FieldPlayer_Wait")
           fieldPlayer:FindSupport()
           return 
        end

      
  end
  --return
    local receiver = fieldPlayer:receiver()
    power = PlayerMaxPassingForce * dot
    
    if ( fieldPlayer:isThreatened()) and fieldPlayer:FindPass(receiver,ballTarget,power,PlayerMinPassDist) then
      local bool = fieldPlayer:FindPass(receiver,ballTarget,power,PlayerMinPassDist)
      ballTarget = fieldPlayer:returnNoise(ballTarget)
      local kickDirection = fieldPlayer:kickDirection(ballTarget, ballPos)
     fieldPlayer:KickBall(kickDirection, power)
      
      fieldPlayer:sendPassMessage(receiver, ballTarget);
      --fieldPlayer:FindSupport()
    else
    
      fieldPlayer:FindSupport()
      fieldPlayer:changeState("State_FieldPlayer_Dribble")

    end
end

  
State_FieldPlayer_KickBall["Exit"] = function(fieldPlayer)
	print("Lua Says Exit")
end


State_FieldPlayer_KickBall["OnMessage"] = function(fieldPlayer, telegram)
	return false;
end

-------------------------------------------------------------------------------

-- create the Kick Ball state


-------------------------------------------------------------------------------
State_FieldPlayer_Dribble = {}


State_FieldPlayer_Dribble["Enter"] = function(fieldPlayer)
   
   fieldPlayer:SetControllingPlayer(fieldPlayer:GetOwnReference())
   
end


State_FieldPlayer_Dribble["Execute"] = function(fieldPlayer)
 
  local dot = fieldPlayer:GetFacing()
  --local dot = -1
 
  if ( dot < 0) then
   
   local direction = fieldPlayer:GetHeading()
   
   local angle = fieldPlayer:AngleBetweenPlayerAndHeading(direction)
   
   fieldPlayer:RotateAroundOrigin(direction,angle)
   
   local kickingForce = 0.8
  
   fieldPlayer:KickBall(direction,kickingForce)
   
  else
    
   fieldPlayer:KickBall(fieldPlayer:GetHomeGoalFacing(),PlayerMaxDribbleForce)
    
  end
 fieldPlayer:changeState("State_FieldPlayer_Chase")
end

  
State_FieldPlayer_Dribble["Exit"] = function(fieldPlayer)
	print("Lua Says Exit")
end


State_FieldPlayer_Dribble["OnMessage"] = function(fieldPlayer, telegram)
 
  
	return false;
end

-------------------------------------------------------------------------------

-- create the Kick Ball state


-------------------------------------------------------------------------------
State_FieldPlayer_ReceiveBall = {}


State_FieldPlayer_ReceiveBall["Enter"] = function(fieldPlayer)
    print ("receiver enter")
    fieldPlayer:SetReceiver(fieldPlayer:GetOwnReference())
    fieldPlayer:SetControllingPlayer(fieldPlayer:GetOwnReference())
    
    local PassThreatRadius = 70.0
    
    if (fieldPlayer:InHotRegion() or (fieldPlayer:RandomFloat() < PlayerChanceOfUsingArriveTypeReceiveBehavior) )  and (fieldPlayer:isOpponentWithinRadius(fieldPlayer:GetOwnPos(),PassThreatRadius) == false ) then
        
        fieldPlayer:ArriveOn()
        
      else
        fieldPlayer:pursuitOn()
        
      end
    
    
end


State_FieldPlayer_ReceiveBall["Execute"] = function(fieldPlayer)
   print ("receiver execute")
  if fieldPlayer:BallWithinReceivingRange()  or fieldPlayer:inControl() then
     fieldPlayer:changeState("State_FieldPlayer_Chase")
     return
  end
  
  if fieldPlayer:pursuitOn() then
    fieldPlayer:SteeringSetTarget(fieldPlayer:BallPos())
  end
  
  if fieldPlayer:AtTarget() then
    fieldPlayer:ArriveOff()
    fieldPlayer:pursuitOff()
    fieldPlayer:TrackBall()
    local vector = fieldPlayer:CreateVector(0,0)
    fieldPlayer:SetVelocity(vector)
  end
  
end

  
State_FieldPlayer_ReceiveBall["Exit"] = function(fieldPlayer)
     print ("receiver exit")

  fieldPlayer:ArriveOff()
  fieldPlayer:pursuitOff()
  
  fieldPlayer:SetReceiver(fieldPlayer:NULLPOINTER())
end


State_FieldPlayer_ReceiveBall["OnMessage"] = function(fieldPlayer, telegram)
	return false;
end





