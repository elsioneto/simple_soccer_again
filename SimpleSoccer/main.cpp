//include the libraries
#pragma comment(lib, "lua5.3.lib")
#pragma comment(lib, "luabind.lib")
#pragma warning (disable:4786)
#include <windows.h>
#include <time.h>
#include <iostream>

#include <windows.h>
#include <stdio.h>
#include <fcntl.h>
#include <io.h>
#include <iostream>
#include <fstream>

#include "constants.h"
#include "misc/utils.h"
#include "Time/PrecisionTimer.h"
#include "SoccerPitch.h"
#include "FieldPlayerStates.h"
#include "misc/Cgdi.h"
#include "ParamLoader.h"
#include "Resource.h"
#include "Goalkeeper.h"
#include "misc/WindowUtils.h"
#include "debug/DebugConsole.h"
#include "Messaging/Telegram.h"
#include "Messaging/MessageDispatcher.h"
#include "SoccerMessages.h"

#ifdef LUA
extern "C"
{
	#include <lua.h>
	#include <lualib.h>
	#include <lauxlib.h>
}
//include the luabind headers. Make sure you have the paths set correctly
//to the lua, luabind and Boost files.
#include <luabind/luabind.hpp>
using namespace luabind;
#include "LuaHelperFunctions.h"

#include <map>
#include <string>
#include "2D/Vector2D.h"

std::map<std::string, lua_State *> g_LuaStates;

#endif
#include "Game/BaseGameEntity.h"
#include "SoccerTeam.h"
#include "FieldPlayer.h"
#include "Goalkeeper.h"

#include "FSM/ScriptedStateMachine.h"

typedef ScriptedStateMachine<FieldPlayer> PlayerFSM;
typedef ScriptedStateMachine<SoccerTeam> TeamFSM;
typedef ScriptedStateMachine<GoalKeeper> GoalFSM;

void RegisterScriptedStateMachineWithLua(lua_State *lua) {
	module(lua)
		[
			class_<PlayerFSM>("ScriptedStateMachine")

			.def("ChangeState", &PlayerFSM::ChangeState)
			.def("CurrentState", &PlayerFSM::CurrentState)
			.def("SetCurrentState", &PlayerFSM::SetCurrentState)
			.def("HandleMessage", &PlayerFSM::HandleMessage)

			,class_<GoalFSM>("ScriptedStateMachine")

			.def("ChangeState", &GoalFSM::ChangeState)
			.def("CurrentState", &GoalFSM::CurrentState)
			.def("SetCurrentState", &GoalFSM::SetCurrentState)
			.def("HandleMessage", &GoalFSM::HandleMessage)

			,class_<TeamFSM>("ScriptedStateMachine")

			.def("ChangeState", &TeamFSM::ChangeState)
			.def("CurrentState", &TeamFSM::CurrentState)
			.def("SetCurrentState", &TeamFSM::SetCurrentState)
			.def("HandleMessage", &TeamFSM::HandleMessage)
		];
}

// Constructor methods for LUA
void RegisterVector2D(lua_State *lua) {
	module(lua)[
		class_<Vector2D>("Vector2D")
			.def(constructor<double, double>())
	];
}

void RegisterTelegram(lua_State *lua)
{
	module(lua)[
		class_<Telegram>("Telegram")
			.def(constructor<double, int, int, int, void*>())
	];
}

void RegisterDispatcher(lua_State *lua)
{
	module(lua)[
		class_<MessageDispatcher>("Telegram")
			
	];
}



//--------------------------------- Globals ------------------------------
//
//------------------------------------------------------------------------

char* g_szApplicationName = "Simple Soccer";
char* g_szWindowClassName = "MyWindowClass";

SoccerPitch* g_SoccerPitch;
lua_State* teamLuaStates;
lua_State* fieldPlayerLuaStates;
lua_State* goalkeeperLuaStates;

//create a timer
PrecisionTimer timer(Prm.FrameRate);


//used when a user clicks on a menu item to ensure the option is 'checked'
//correctly
void CheckAllMenuItemsAppropriately(HWND hwnd)
{
   CheckMenuItemAppropriately(hwnd, IDM_SHOW_REGIONS, Prm.bRegions);
   CheckMenuItemAppropriately(hwnd, IDM_SHOW_STATES, Prm.bStates);
   CheckMenuItemAppropriately(hwnd, IDM_SHOW_IDS, Prm.bIDs);
   CheckMenuItemAppropriately(hwnd, IDM_AIDS_SUPPORTSPOTS, Prm.bSupportSpots);
   CheckMenuItemAppropriately(hwnd, ID_AIDS_SHOWTARGETS, Prm.bViewTargets);
   CheckMenuItemAppropriately(hwnd, IDM_AIDS_HIGHLITE, Prm.bHighlightIfThreatened);
}


// maximum mumber of lines the output console should have
static const WORD MAX_CONSOLE_LINES = 500;


//Thanks to: http://www.halcyon.com/~ast/dload/guicon.htm
// Call this exactly once - it gives you a console you can output to. This is
// necessary to get the lua print() function working.
void RedirectIOToConsole()
{
	//Flag to ensure this only gets called once.
	static bool firstTime = true;
	if (!firstTime) {
		return;
	}
	firstTime = false;

	int hConHandle;
	long lStdHandle;
	CONSOLE_SCREEN_BUFFER_INFO coninfo;
	FILE *fp;

	// allocate a console for this app
	AllocConsole();

	// set the screen buffer to be big enough to let us scroll text
	GetConsoleScreenBufferInfo(GetStdHandle(STD_OUTPUT_HANDLE), &coninfo);
	coninfo.dwSize.Y = MAX_CONSOLE_LINES;
	SetConsoleScreenBufferSize(GetStdHandle(STD_OUTPUT_HANDLE), coninfo.dwSize);

	// redirect unbuffered STDOUT to the console
	lStdHandle = (long)GetStdHandle(STD_OUTPUT_HANDLE);
	hConHandle = _open_osfhandle(lStdHandle, _O_TEXT);

	fp = _fdopen(hConHandle, "w");

	*stdout = *fp;

	setvbuf(stdout, NULL, _IONBF, 0);

	// redirect unbuffered STDIN to the console

	lStdHandle = (long)GetStdHandle(STD_INPUT_HANDLE);
	hConHandle = _open_osfhandle(lStdHandle, _O_TEXT);

	fp = _fdopen(hConHandle, "r");
	*stdin = *fp;
	setvbuf(stdin, NULL, _IONBF, 0);

	// redirect unbuffered STDERR to the console
	lStdHandle = (long)GetStdHandle(STD_ERROR_HANDLE);
	hConHandle = _open_osfhandle(lStdHandle, _O_TEXT);

	fp = _fdopen(hConHandle, "w");

	*stderr = *fp;

	setvbuf(stderr, NULL, _IONBF, 0);

	// make cout, wcout, cin, wcin, wcerr, cerr, wclog and clog
	// point to console as well
	std::ios::sync_with_stdio();
}

//---------------------------- WindowProc ---------------------------------
//	
//	This is the callback function which handles all the windows messages
//-------------------------------------------------------------------------

LRESULT CALLBACK WindowProc(HWND   hwnd,
	UINT   msg,
	WPARAM wParam,
	LPARAM lParam)
{

	//these hold the dimensions of the client window area
	static int cxClient, cyClient;

	//used to create the back buffer
	static HDC		hdcBackBuffer;
	static HBITMAP	hBitmap;
	static HBITMAP	hOldBitmap;

	switch (msg)
	{

		//A WM_CREATE msg is sent when your application window is first
		//created
	case WM_CREATE:
	{
		// RedirectIOToConsole();

		//to get get the size of the client window first we need  to create
		//a RECT and then ask Windows to fill in our RECT structure with
		//the client window size. Then we assign to cxClient and cyClient 
		//accordingly
		RECT rect;

		GetClientRect(hwnd, &rect);

		cxClient = rect.right;
		cyClient = rect.bottom;

		//seed random number generator
		srand((unsigned)time(NULL));


		//---------------create a surface to render to(backbuffer)

		//create a memory device context
		hdcBackBuffer = CreateCompatibleDC(NULL);

		//get the DC for the front buffer
		HDC hdc = GetDC(hwnd);

		hBitmap = CreateCompatibleBitmap(hdc,
			cxClient,
			cyClient);


		//select the bitmap into the memory device context
		hOldBitmap = (HBITMAP)SelectObject(hdcBackBuffer, hBitmap);

		//don't forget to release the DC
		ReleaseDC(hwnd, hdc);

		std::cout << "begin lua loading" << std::endl;

		//create lua states
		teamLuaStates = luaL_newstate();
		fieldPlayerLuaStates = luaL_newstate();
		goalkeeperLuaStates = luaL_newstate();

		//open the lua libaries - new in lua5.1
		luaL_openlibs(teamLuaStates);
		luaL_openlibs(fieldPlayerLuaStates);
		luaL_openlibs(goalkeeperLuaStates);

		//open luabind
		open(teamLuaStates);
		open(fieldPlayerLuaStates);
		open(goalkeeperLuaStates);

		//Register the entities so they can be accessed.
		RegisterScriptedStateMachineWithLua(teamLuaStates);
		RegisterScriptedStateMachineWithLua(fieldPlayerLuaStates);
		RegisterScriptedStateMachineWithLua(goalkeeperLuaStates);

		RegisterSoccerTeam(teamLuaStates);

		RegisterVector2D(fieldPlayerLuaStates);
		RegisterVector2D(goalkeeperLuaStates);

		RegisterFieldPlayer(fieldPlayerLuaStates);
		RegisterGoalkeeper(goalkeeperLuaStates);

		RegisterTelegram(fieldPlayerLuaStates);
		RegisterTelegram(goalkeeperLuaStates);
		RegisterTelegram(teamLuaStates);

		RegisterDispatcher(fieldPlayerLuaStates);
		RegisterDispatcher(goalkeeperLuaStates);
		RegisterDispatcher(teamLuaStates);

		RunLuaScript(teamLuaStates, "TeamStates.lua");
		RunLuaScript(fieldPlayerLuaStates, "FieldPlayerStates.lua");
		RunLuaScript(goalkeeperLuaStates, "GoalkeeperStates.lua");

		//luabind::adl::object o = luabind::globals
		g_LuaStates["TeamStates"] = teamLuaStates;
		g_LuaStates["FieldPlayerStates"] = fieldPlayerLuaStates;
		g_LuaStates["GoalkeeperStates"] = goalkeeperLuaStates;

		//Testing Testing
		globals(teamLuaStates)["Blah"]["Test"]();
		globals(fieldPlayerLuaStates)["Blah"]["Test"]();
		globals(goalkeeperLuaStates)["Blah"]["Test"]();
		//bool value = luabind::call_function<bool>( globals(teamLuaStates)["Blah"]["Stuff"]);

		std::cout << "End lua loading" << std::endl;

		g_SoccerPitch = new SoccerPitch(cxClient, cyClient);

		CheckAllMenuItemsAppropriately(hwnd);

	}

	break;

	case WM_COMMAND:
	{
		switch (wParam)
		{
		case ID_AIDS_NOAIDS:

			Prm.bStates = 0;
			Prm.bRegions = 0;
			Prm.bIDs = 0;
			Prm.bSupportSpots = 0;
			Prm.bViewTargets = 0;

			CheckAllMenuItemsAppropriately(hwnd);

			break;

		case IDM_SHOW_REGIONS:

			Prm.bRegions = !Prm.bRegions;

			CheckAllMenuItemsAppropriately(hwnd);

			break;

		case IDM_SHOW_STATES:

			Prm.bStates = !Prm.bStates;

			CheckAllMenuItemsAppropriately(hwnd);

			break;

		case IDM_SHOW_IDS:

			Prm.bIDs = !Prm.bIDs;

			CheckAllMenuItemsAppropriately(hwnd);

			break;


		case IDM_AIDS_SUPPORTSPOTS:

			Prm.bSupportSpots = !Prm.bSupportSpots;

			CheckAllMenuItemsAppropriately(hwnd);

			break;

		case ID_AIDS_SHOWTARGETS:

			Prm.bViewTargets = !Prm.bViewTargets;

			CheckAllMenuItemsAppropriately(hwnd);

			break;

		case IDM_AIDS_HIGHLITE:

			Prm.bHighlightIfThreatened = !Prm.bHighlightIfThreatened;

			CheckAllMenuItemsAppropriately(hwnd);

			break;

		}//end switch
	}

	break;


	case WM_KEYUP:
	{
		switch (wParam)
		{
		case VK_ESCAPE:
		{
			SendMessage(hwnd, WM_DESTROY, NULL, NULL);
		}

		break;

		case 'R':
		{
			delete g_SoccerPitch;

			g_SoccerPitch = new SoccerPitch(cxClient, cyClient);
		}

		break;

		case 'P':
		{
			g_SoccerPitch->TogglePause();
		}

		break;

		}//end switch

	}//end WM_KEYUP

	break;


	case WM_PAINT:
	{

		PAINTSTRUCT ps;

		BeginPaint(hwnd, &ps);

		gdi->StartDrawing(hdcBackBuffer);

		g_SoccerPitch->Render();

		gdi->StopDrawing(hdcBackBuffer);



		//now blit backbuffer to front
		BitBlt(ps.hdc, 0, 0, cxClient, cyClient, hdcBackBuffer, 0, 0, SRCCOPY);

		EndPaint(hwnd, &ps);

	}

	break;

	//has the user resized the client area?
	case WM_SIZE:
	{
		//if so we need to update our variables so that any drawing
		//we do using cxClient and cyClient is scaled accordingly
		cxClient = LOWORD(lParam);
		cyClient = HIWORD(lParam);

		//now to resize the backbuffer accordingly. First select
		//the old bitmap back into the DC
		SelectObject(hdcBackBuffer, hOldBitmap);

		//don't forget to do this or you will get resource leaks
		DeleteObject(hBitmap);

		//get the DC for the application
		HDC hdc = GetDC(hwnd);

		//create another bitmap of the same size and mode
		//as the application
		hBitmap = CreateCompatibleBitmap(hdc,
			cxClient,
			cyClient);

		ReleaseDC(hwnd, hdc);

		//select the new bitmap into the DC
		SelectObject(hdcBackBuffer, hBitmap);

	}

	break;

	case WM_DESTROY:
	{

		//clean up our backbuffer objects
		SelectObject(hdcBackBuffer, hOldBitmap);

		DeleteDC(hdcBackBuffer);
		DeleteObject(hBitmap);

		// kill the application, this sends a WM_QUIT message  
		PostQuitMessage(0);
	}

	break;

	}//end switch

	 //this is where all the messages not specifically handled by our 
	 //winproc are sent to be processed
	return DefWindowProc(hwnd, msg, wParam, lParam);
}

//-------------------------------- WinMain -------------------------------
//
//	The entry point of the windows program
//------------------------------------------------------------------------
int WINAPI WinMain(HINSTANCE hInstance,
	HINSTANCE hPrevInstance,
	LPSTR     szCmdLine,
	int       iCmdShow)
{
	RedirectIOToConsole();
	//handle to our window
	HWND						hWnd;

	//our window class structure
	WNDCLASSEX     winclass;

	// first fill in the window class stucture
	winclass.cbSize = sizeof(WNDCLASSEX);
	winclass.style = CS_HREDRAW | CS_VREDRAW;
	winclass.lpfnWndProc = WindowProc;
	winclass.cbClsExtra = 0;
	winclass.cbWndExtra = 0;
	winclass.hInstance = hInstance;
	winclass.hIcon = LoadIcon(hInstance, MAKEINTRESOURCE(IDI_ICON1));
	winclass.hCursor = LoadCursor(NULL, IDC_ARROW);
	winclass.hbrBackground = NULL;
	winclass.lpszMenuName = MAKEINTRESOURCE(IDR_MENU1);
	winclass.lpszClassName = g_szWindowClassName;
	winclass.hIconSm = LoadIcon(hInstance, MAKEINTRESOURCE(IDI_ICON1));

	//register the window class
	if (!RegisterClassEx(&winclass))
	{
		MessageBox(NULL, "Registration Failed!", "Error", 0);

		//exit the application
		return 0;
	}

	//create the window and assign its ID to hwnd    
	hWnd = CreateWindowEx(NULL,                 // extended style
		g_szWindowClassName,  // window class name
		g_szApplicationName,  // window caption
		WS_OVERLAPPED | WS_VISIBLE | WS_CAPTION | WS_SYSMENU,
		GetSystemMetrics(SM_CXSCREEN) / 2 - WindowWidth / 2,
		GetSystemMetrics(SM_CYSCREEN) / 2 - WindowHeight / 2,
		WindowWidth,     // initial x size
		WindowHeight,    // initial y size
		NULL,                 // parent window handle
		NULL,                 // window menu handle
		hInstance,            // program instance handle
		NULL);                // creation parameters

							  //make sure the window creation has gone OK
	if (!hWnd)
	{
		MessageBox(NULL, "CreateWindowEx Failed!", "Error!", 0);
	}

	//start the timer
	timer.Start();

	MSG msg;

	//enter the message loop
	bool bDone = false;
	//For some reason this swallows the first message until another message is printed.
	//You can use the debug console for C++ stuff and leave the regular console for lua stuff.
	//debug_con << "Entering Main Loop" << "" << " " << "";
	//debug_con << " " << "";
	while (!bDone)
	{


		while (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE))
		{
			if (msg.message == WM_QUIT)
			{
				// Stop loop if it's a quit message
				bDone = true;
			}

			else
			{
				TranslateMessage(&msg);
				DispatchMessage(&msg);
			}
		}

		if (timer.ReadyForNextFrame() && msg.message != WM_QUIT)
		{
			//update game states
			g_SoccerPitch->Update();

			//render 
			RedrawWindow(hWnd, true);

			Sleep(2);
		}

	}//end while

	delete g_SoccerPitch;

	UnregisterClass(g_szWindowClassName, winclass.hInstance);

	return msg.wParam;
}


